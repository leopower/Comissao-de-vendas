USE DBCONCESSIONARIA
GO


SELECT
    VEI001.IdeMarca
,   VEI002.IdeModelo
,   VEI004.IdeModeloVersao
,   VEI001.NmeMarca
,   VEI002.NmeModelo
,   VEI004.NroAno
,   VEI003.NmeCombustivel
,   VEI004.VlrPrecoTabelado

    FROM VEI001_MARCA VEI001
    
    INNER JOIN VEI002_MODELO VEI002
        ON VEI002.IdeMarca = VEI001.IdeMarca

    INNER JOIN VEI004_MODELO_VERSAO VEI004
        ON VEI004.IdeModelo = VEI002.IdeModelo

    INNER JOIN VEI003_COMBUSTIVEL VEI003
        ON VEI003.IdeCombustivel = VEI004.IdeCombustivel

    ORDER BY VEI001.IdeMarca, VEI002.IdeModelo, VEI004.NroAno, VEI003.IdeCombustivel


    SELECT
    VND001.NmeVendedor,
    QtdVendas               = COUNT(*),
    Sum( CASE WHEN VND002.StaValeCombustivel = 1 THEN 1 ELSE 0 END) StaValeCombustivel ,
    VlrTotalVendas          = SUM(VND002.VlrPrecoVenda),
    PctMargemVendas         = CAST((SUM(VND002.VlrPrecoVenda) / SUM(VEI004.VlrPrecoTabelado) - 1) * 100 AS NUMERIC(6, 4))
    

    FROM VND002_VENDA VND002

        INNER JOIN VND001_VENDEDOR VND001
        ON VND001.IdeVendedor = VND002.IdeVendedor

         INNER JOIN VEI004_MODELO_VERSAO VEI004
        ON VEI004.IdeModeloVersao = VND002.IdeModeloVersao

        GROUP BY   VND001.NmeVendedor

        ORDER BY NmeVendedor


    SELECT
    VlrTotalVendas          = SUM(VND002.VlrPrecoVenda)
,   VlrTotalTabeladoVendas  = SUM(VEI004.VlrPrecoTabelado)
,   PctMargemVendas         = CAST((SUM(VND002.VlrPrecoVenda) / SUM(VEI004.VlrPrecoTabelado) - 1) * 100 AS NUMERIC(6, 4))
,   QtdVendas               = COUNT(*)
,   VlrMedioVendas          = CAST(SUM(VND002.VlrPrecoVenda) / COUNT(*) AS DECIMAL(10,2))

    FROM VND002_VENDA VND002

    INNER JOIN VEI004_MODELO_VERSAO VEI004
        ON VEI004.IdeModeloVersao = VND002.IdeModeloVersao



       USE DBCONCESSIONARIA
GO

CREATE OR ALTER PROCEDURE ListarComissoesVendedores
AS

    -- Altere esta procedure para retornar a lista
    -- com o resultado das comiss�es por vendedor

     SELECT
    VND001.NmeVendedor,
    QtdVendas               = COUNT(*),
    Sum( CASE WHEN VND002.StaValeCombustivel = 1 THEN 1 ELSE 0 END) StaValeCombustivel ,
    VlrTotalVendas          = SUM(VND002.VlrPrecoVenda),
    PctMargemVendas         = CAST((SUM(VND002.VlrPrecoVenda) / SUM(VEI004.VlrPrecoTabelado) - 1) * 100 AS NUMERIC(6, 4))
    

    FROM VND002_VENDA VND002

        INNER JOIN VND001_VENDEDOR VND001
        ON VND001.IdeVendedor = VND002.IdeVendedor

         INNER JOIN VEI004_MODELO_VERSAO VEI004
        ON VEI004.IdeModeloVersao = VND002.IdeModeloVersao

        GROUP BY   VND001.NmeVendedor

        ORDER BY NmeVendedor

GO