﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Comissao_de_vendas.Models
{
    public class VendasViewModel
    {
        [Column(name: "NmeVendedor", Order = 1)]
        public int NomeVendedor { get; set; }

        [Column(name: "QtdVendas", Order = 2)]
        public int QuantidadeVendas { get; set; }

        [Column(name: "StaValeCombustivel", Order = 3)]
        public int ValeCombustivel { get; set; }

        [Column(name: "VlrTotalVendas", Order = 4)]
        public int TotalVendas { get; set; }

        [Column(name: "PctMargemVendas", Order = 5)]
        public string MargemVendas { get; set; }

    }
}